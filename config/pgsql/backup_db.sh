
# 在宿主机上执行脚本：docker exec -it postgresql bash -c "bash /var/backup/pgsql/backup_db.sh"

# 备份数据库
backup_dir="/var/lib/postgresql/data/backup" # 备份地址
backup_file="db_$(date +%Y%m%d%H%M%S).dump" # 备份文件名

# 定义数据库名和登录信息
db_name="zyp-test"
db_owner="postgres"
db_user="postgres"
db_pass="123456"


# 备份数据库 参数文档：https://www.postgresql.org/docs/current/app-pgdump.html
pg_dump --file $backup_dir/$backup_file --host 127.0.0.1 --port 5432 --username $db_user --dbname $db_name -c --role $db_owner --format=c --blobs --encoding "UTF8"