#!/bin/bash

read -p "Do you want to pull it ?(y/n): " answer

if [ "$answer" == "y" ]; then
    git pull
fi

bash many-start.sh \
&& docker system prune -f	