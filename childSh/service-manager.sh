#!/bin/bash

###########################################
# 服务管理脚本
# 本脚本用于管理和启动 docker-compose 服务
# 
# 主要功能：
# - 根据服务类型加载对应的服务配置
# - 提供交互式服务选择界面
# - 管理服务的启动和重建
###########################################

###########################################
# 从父脚本获取服务类型参数
# $1: 服务类型(如：logs/esapm/ai等)
###########################################
SERVICE_TYPE=$1

###########################################
# 加载服务配置文件
# 配置文件包含各服务组的映射关系
# 使用source命令将配置加载到当前shell环境
###########################################
source ./childSh/services-config.sh

###########################################
# 根据服务类型选择对应的配置
# 将选定类型的服务配置加载到dict数组中
###########################################
case $SERVICE_TYPE in
    "db")
        # 加载数据库相关服务配置
        declare -A dict=( "${!db_services[@]}" )
        for key in "${!db_services[@]}"; do
            dict[$key]=${db_services[$key]}
        done
        ;;
    "smallservice")
        # 加载小服务相关服务配置
        declare -A dict=( "${!smallservice_services[@]}" )
        for key in "${!smallservice_services[@]}"; do
            dict[$key]=${smallservice_services[$key]}
        done
        ;;
    "logs")
        # 加载日志相关服务配置
        declare -A dict=( "${!logs_services[@]}" )
        for key in "${!logs_services[@]}"; do
            dict[$key]=${logs_services[$key]}
        done
        ;;
    "git")
        # 加载git相关服务配置
        declare -A dict=( "${!git_services[@]}" )
        for key in "${!git_services[@]}"; do
            dict[$key]=${git_services[$key]}
        done
        ;;
    "es")
        # 加载es相关服务配置
        declare -A dict=( "${!es_services[@]}" )
        for key in "${!es_services[@]}"; do
            dict[$key]=${es_services[$key]}
        done
        ;;
    "tools")
        # 加载tools相关服务配置
        declare -A dict=( "${!tools_services[@]}" )
        for key in "${!tools_services[@]}"; do
            dict[$key]=${tools_services[$key]}
        done
        ;;
    "observability")
        # 加载observability相关服务配置
        declare -A dict=( "${!observability_services[@]}" )
        for key in "${!observability_services[@]}"; do
            dict[$key]=${observability_services[$key]}
        done
        ;;
    "ai")
        # 加载AI相关服务配置
        declare -A dict=( "${!ai_services[@]}" )
        for key in "${!ai_services[@]}"; do
            dict[$key]=${ai_services[$key]}
        done
        ;;
    "milvus")
        # 加载milvus相关服务配置
        declare -A dict=( "${!milvus_services[@]}" )
        for key in "${!milvus_services[@]}"; do
            dict[$key]=${milvus_services[$key]}
        done
        ;;
    "esapm")
        # 加载Elastic Stack相关服务配置
        declare -A dict=( "${!esapm_services[@]}" )
        for key in "${!esapm_services[@]}"; do
            dict[$key]=${esapm_services[$key]}
        done
        ;;
    *)
        # 处理未知的服务类型
        echo -e "\033[31m未找到服务类型: $SERVICE_TYPE \033[0m"
        exit 1
        ;;
esac

###########################################
# 显示可用服务列表
# 以彩色输出方式展示所有可选服务
###########################################
echo -e "\033[33m可用的服务列表: \033[0m"
for key in "${!dict[@]}"; do
    echo -e "\033[31m $key --> ${dict[$key]} \033[0m"
done

###########################################
# 用户输入处理
# 允许用户选择多个服务
# 输入'e'结束选择
###########################################
declare -a list=()  # 存储用户选择的服务编号
inde=0              # 数组索引

echo -e "\033[33m请输入服务编号，输入e结束: \033[0m"
while read input; do
    if [ "$input" == "e" ]; then
        break
    else
        list[$inde]=$input
        let inde+=1
    fi
done

###########################################
# 输入验证
# 检查用户是否输入了服务编号
###########################################
if [ ${#list[@]} -le 0 ]; then
    echo -e "\033[31m未输入任何服务编号，退出脚本 \033[0m"
    exit 2
fi

###########################################
# 显示选中的服务
# 验证用户选择的服务是否有效
###########################################
echo -e "\033[33m您选择了以下服务: \033[0m"
for item in "${list[@]}"; do
    if [[ -z ${dict[$item]} ]]; then
        echo -e "\033[31m $item 不存在 \033[0m"
    else
        echo "$item --> ${dict[$item]}"
    fi
done

###########################################
# 执行确认
# 要求用户确认是否执行服务启动
###########################################
echo -e -n "\033[33m是否确认开始处理 (y/n)? \033[0m"
read confirm

###########################################
# 服务启动处理
# 使用docker-compose启动选中的服务
# 
# 参数说明：
# --build: 每次都重新构建容器
# -d: 后台运行容器
# --force-recreate: 强制重新创建容器
# --remove-orphans: 删除不在compose文件中定义的服务容器
###########################################
if [ "$confirm" == "y" ]; then
    for item in "${list[@]}"; do
        if [[ -z ${dict[$item]} ]]; then
            continue
        else
            echo "处理服务: ${dict[$item]}"
            docker compose -f ./docker-compose.${SERVICE_TYPE}.yaml up --build -d --force-recreate ${dict[$item]}
            # docker compose -f ./docker-compose.${SERVICE_TYPE}.yaml up --build -d --force-recreate --remove-orphans ${dict[$item]}
        fi
    done
    echo -e "\033[32m构建完成 \033[0m"
else
    echo -e "\033[31m已取消操作 \033[0m"
fi 