#!/bin/bash

###########################################
# 服务配置文件
# 本文件用于集中管理所有docker服务的配置信息
# 使用关联数组(类似字典)来存储服务编号和服务名称的映射关系
###########################################

###########################################
# 使用说明：
# 1. 此配置文件由 service-manager.sh 脚本读取
# 2. 每个服务组都使用独立的关联数组存储
# 3. 数组的键为服务编号（3位数字）
# 4. 数组的值为对应的 docker-compose 服务名称
# 5. 添加新服务时，需要确保：
#    - 服务编号唯一（在同一组内）
#    - 服务名称与 docker-compose 文件中的服务名称一致
########################################### 

declare -A db_services=( 
    ["001"]="mysql"
    ["002"]="sqlserver"
    ["003"]="postgresql"
    ["004"]="mongo"
    ["005"]="redis"
    ["006"]="clickhouse"
    ["007"]="qdrant"
)

declare -A smallservice_services=( 
    ["001"]="fastdfsstorage"
    ["002"]="agileconfig"
    ["003"]="myconsul"
    ["004"]="quartzjob"
    ["005"]="nginx_ui"
    ["006"]="win_minio"
    ["007"]="linux_minio"
    ["008"]="old_win_minio"
    ["009"]="rabbitmq"
)

declare -A logs_services=(
    ["001"]="myexceptionLess"
    ["002"]="seqcli" 
    ["003"]="seq"
)

declare -A git_services=( 
    ["001"]="gitlab"
    ["002"]="gitlab-runner"
    ["003"]="mygogs"
    ["004"]="jenkins"
)

declare -A es_services=( 
    ["001"]="elasticsearch"
    ["002"]="kibana"
)

declare -A tools_services=( 
    ["001"]="dozzle"
    ["002"]="calibre-web"
    ["003"]="jingdong"
    ["004"]="transmission"
    ["005"]="jellyfin"
)

declare -A observability_services=( 
    ["001"]="prometheus"
    ["002"]="grafana"
    ["003"]="aspire_dashboard"
)

declare -A ai_services=( 
    ["001"]="ollama"
    ["002"]="maxkb"
    ["003"]="anything-llm"
)

declare -A milvus_services=(
    ["001"]="milvus"
)

declare -A esapm_services=(
    ["001"]="elasticsearch"
    ["002"]="apm-server"
)
