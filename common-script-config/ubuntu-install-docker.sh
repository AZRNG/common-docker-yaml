#!/bin/bash

# ubuntu一键安装docker脚本

echo -e "\033[33m ubuntu install docker \033[0m"

# 卸载旧版本
echo -e "\033[33m Uninstall the old version \033[0m"
sudo apt remove docker docker-engine docker.io containerd runc

# 首先更新apt包索引并安装包以允许apt通过https使用存储库
echo -e "\033[33m update pack \033[0m"
sudo apt update
sudo apt upgrade -y
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release

# 添加docker官网的gpg 密钥
echo -e "\033[33m Add a key \033[0m"
# sudo mkdir -p /etc/apt/keyrings
# todo 可能是这里需要确认所以导致安装一直失败
# curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -

# 设置存储库
echo -e "\033[33m Setting up the repository \033[0m"
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] http://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# 安装Docker Engine、containerd和Docker Compose
echo -e "\033[33m install docker \033[0m"
sudo apt install -y docker-ce docker-ce-cli containerd.io

#安装docker-compose,最新版本需要手动查询一下
echo -e "\033[33m install docker-compose \033[0m"
sudo curl -L https://github.com/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose