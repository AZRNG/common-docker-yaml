#!/bin/bash
# centos 一键安装docker的脚本

echo -e "\033[33m install docker sh \033[0m"

#1. 安装Docker
# 设置yum源
echo -e "\033[33m Set the yum source \033[0m"
sudo yum -y install yum-utils
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

#yum包更新到最新
echo -e "\033[33m update version \033[0m"
sudo yum update -y

#安装Docker最新版
echo -e "\033[33m install docker-ce \033[0m"
sudo yum install docker-ce -y

#设置Docker自启动
echo -e "\033[33m setting self-turn-on \033[0m"
sudo systemctl enable  docker

#启动Docker
echo -e "\033[33m start docker \033[0m"
sudo systemctl start docker

#配置国内镜像 /etc/docker/daemon.json
echo -e "\033[33m Set up aliyuncs mirror \033[0m"
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://i9it9b0k.mirror.aliyuncs.com"]
}
EOF
#加载配置文件,ReStart
sudo systemctl daemon-reload
sudo systemctl restart docker

#安装docker-compose,最新版本需要手动查询一下
echo -e "\033[33m install docker-compose \033[0m"
sudo curl -L https://github.com/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
