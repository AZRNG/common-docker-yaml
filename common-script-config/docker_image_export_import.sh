#!/bin/bash

# docker image export import

echo "Please select action:"
echo "1) Export Images"
echo "2) Import Images"
read -p "Please enter your choice (1/2)： " choice

case $choice in
    1)
        # export images
        read -p "Please enter the image ID: " image_id
        read -p "Please enter the export file name: " export_path
        echo "export..."
        # docker export $container_id > $export_path
        docker save $image_id -o $export_path
        echo "Export complete"
        ;;
    2)
        # import images
        read -p "Please enter the image ID: " image_id
        read -p "Please enter the import name: " image_name
        read -p "Please enter the import path: " import_path
        echo "import..."
        # docker import $import_path $import_name
        docker load -i $import_path
        docker tag $image_id $image_name
        echo "imports closure。"
        ;;
    *)
        echo "Invalid selection, the script will exit。"
        exit 1
        ;;
esac