#!/bin/bash

# readme
# 本文档暂时已经弃用，留档只是备用

#声明关联数组 类似于字典，键唯一
declare -A dict=( 
    ["001"]="mysql"
    ["002"]="rabbitmq"
    ["003"]="redis"
    ["004"]="mongo"
    ["005"]="agileconfig"
    ["006"]="jenkins"
)


# 输出并将字体设置为黄色
echo -e "\033[33m Output all the services: \033[0m"
# !可以让除数字典对应的key
for key in $(echo ${!dict[*]})
do
    echo -e "\033[31m $key --> ${dict[$key]} \033[0m"
done


#接收用户输入数据

# 存储用户输入的数组
declare -a list=()

# 定义变量 用来存储索引
inde=0
echo -e "\033[33m please input your service port,and input e to finish: \033[0m"

# read 用来接收输入的值  read后面的变量就是存储接收到的值
while read input
do
    if [ $input == "e" ]
    then
        break
    else
        list[$inde]=$input
        let inde+=1
    fi
done

#判断输入长度 如果没有输入就退出
len=${#list[@]}
# -le：类似于<=
if [ $len -le 0 ]
then
    echo -e "\033[31m your input is empty,and exit shell \033[0m"
    exit 2
fi

#显示用户要处理的服务列表
echo -e "\033[33m You will handle the following services : \033[0m"

# 循环输出数组的内容
for item in ${list[*]}
do
    # -z 代表检查后面的内容是否为0；-n为检查内容是否大于0
    if [[ -z ${dict[$item]} ]]
    then
        # 如果上面选择的内容的key在字典中不存在，那么就输入内容提示未找到
        echo -e "\033[31m $item not exist \033[0m"
    else
        echo "$item --> ${dict[$item]}"
    fi
done

# 执行真实的逻辑  请确认是否开始处理
echo -e -n "\033[33m please confirm whether to start processing,y=>yes? \033[0m"
read confirm
# 如果输入y 就开始执行生成docker容器
if [ $confirm == "y" ]
then
    for item in ${list[*]}
    do
        echo  ${dict[$item]}
        # 如果当前key在字典中不存在，那么就跳过继续
        if [[ -z ${dict[$item]} ]]
        then
            continue
        else
            # --force-recreate 创建容器即使没有改变它们的配置和镜像
            # 运行指定文件 docker-compose -f docker-compose.${dictgroup[$port]}.yml up --build -d --force-recreate ${dict[$port]}
            docker-compose up --build -d --force-recreate ${dict[$item]}
        fi
    done
    echo -e "\033[32m build finished, good luck \033[0m"
else
    # 输入其他内容就取消执行
    echo -e "\033[31m you canceled operation \033[0m"
    fi