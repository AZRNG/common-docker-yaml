#!/bin/bash

# readme
# 本文档执行的docker-compose 为多个文件

echo -e "\033[33m Select the file you want to execute: \033[0m"
declare -A files=( 
    ["001"]="db"
    ["002"]="smallservice"
    ["003"]="logs"
    ["004"]="git"
    ["005"]="es"
    ["006"]="tools"
    ["007"]="observability"
    ["008"]="ai"
    ["009"]="milvus" # milvus 向量数据库
)

# 输出并将字体设置为黄色
echo -e "\033[33m Output all the file: \033[0m"
# !可以让除数字典对应的key
for key in $(echo ${!files[*]})
do
    echo -e "\033[31m $key --> ${files[$key]} \033[0m"
done

echo -e "\033[33m please input your file num \033[0m"
read inputfile
if [ $inputfile -le 0 ]
then 
    echo -e "\033[31m your input is empty,and exit shell \033[0m"
    exit 2
fi

if [[ -z ${files[$inputfile]} ]]
then 
    echo -e "\033[31m The number you entered was not found,and exit shell \033[0m"
    exit 2
fi

bash ./childSh/service-manager.sh ${files[$inputfile]}